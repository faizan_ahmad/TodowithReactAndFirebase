import React, { Component } from 'react'


class TodoForms extends Component {
    state = {
        title : ''
    }

    handleChange = (e) => {
        this.setState({
            title: e.target.value
        })
    }

    handleSubmit= (e) => {
        e.preventDefault()
        this.props.addTodo(this.state.title)
        this.setState({
            title:''
        })
    }
    render () {
        return (
            <div className='todo-form'>
                <form className="form" action="" autoComplete="off" onSubmit={this.handleSubmit}>
                    <div className='form-input'>
                        <input type="text" placeholder="Enter Todo" 
                            value={this.state.title}
                        onChange={this.handleChange} />
                    </div>
                    <div className='form-btn'> 
                    <button type="submit">
                        <i className="das fa-plus"></i>
                    </button>
                    </div>
                </form>
            </div>
        )
    }
}

export default TodoForms