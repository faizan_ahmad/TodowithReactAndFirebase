import React, { Component } from 'react'
import TodosItems from './TodosItems'

class RetrivedTodos extends Component {
    render () {
        return this.props.todos.map((retrivedTodos) =>(
            <TodosItems key={retrivedTodos.id} todos={retrivedTodos}
            markComplete={this.props.markComplete}
            deleteTodo={this.props.deleteTodo}
            />
        ))
    }
}

export default RetrivedTodos