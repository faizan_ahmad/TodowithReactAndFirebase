import React, { Component } from 'react'

class TodosItems  extends Component {
    getStrikeMark = () => {
        return {
            textDecoration:this.props.todos.Completed ?
                'underline': 'none'
        }
    }
    render () {
        const { id , title } = this.props.todos
        return (
            <div className='main-todos-div' >
                <div className='todo-div' style={this.getStrikeMark()}>
                    <input type="checkbox" className='checkbox-round' 
                    onChange={this.props.markComplete.bind(this,id)}/> 
                    <span>{title}</span>
                </div>
                <div className='btn-div'>
                    <button onClick={this.props.deleteTodo.bind(this,id)}>
                        <i className="fas fa-trash"></i>    
                     </button>
                </div>
            </div>
                

        )
    }
}

export default TodosItems