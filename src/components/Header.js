import React, { Component } from 'react'

class Header extends Component {
    render () {
        const date= new Date();
        const todayDate = date.getDate();
        const month = date.toLocaleString('default',{month:'long'});
        const year = date.getFullYear();
        const day = date.toLocaleDateString('default',{weekday:'long'});
        return(
            <div className='main-header-div'>
                    <div className='background-div'> </div>
                    <div className='date-month-div'> </div>
                    <span>{todayDate}</span>
                    <span>{month}</span>
                    <span>{year}</span>
                    <span>{day}</span>
                    </div>
        )
    }
}

export default Header