import firebase from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyAfBFuKGL0OWFwh9zxumNCVwAFDFXGNBw0",
    authDomain: "todoapp-19794.firebaseapp.com",
    databaseURL: "https://todoapp-19794.firebaseio.com",
    projectId: "todoapp-19794",
    storageBucket: "todoapp-19794.appspot.com",
    messagingSenderId: "659209035827",
    appId: "1:659209035827:web:845fdceb4e5ebba5d28346",
    measurementId: "G-WQ1YH10CCS"
  };
// eslint-disable-next-line
  const fire = firebase.initializeApp(firebaseConfig)// eslint-disable-next-line

  export default firebase