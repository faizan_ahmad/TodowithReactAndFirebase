import React from 'react';
import Header from './components/Header';
import RetrivedTodos from './components/RetrivedTodos';
import TodoForms from './components/TodoForms';
import firebase from './firebase';

class App extends React.Component {
  state = {
    todos:[
      // {id:1, title:'get haircut',completed: false},
      // {id:2, title:'learn react',completed: false},
      // {id:3, title:'chaaa',completed: false},
    ]
  }

  componentDidMount(){
    const previousTodos = this.state.todos;
    firebase.database().ref('todos').on('child_added',child=>{
      previousTodos.push({
        id:child.key,
        title: child.val().title,
        completed:false
      })

      this.setState({
        todos:previousTodos
      })

      firebase.database().ref('todos').on('child_removed',child=>{
        for(var i=0; i<previousTodos.length; i++){
            if(previousTodos[i].id === child.key){
              previousTodos.splice(i,1)
            }
        }
        this.setState({
          todos:previousTodos
        })
      })
    })
  }
  markComplete=(id) =>{
    // this.setState({
    //   todos: this.state.todos.map((myTodo)=>{
    //     if(myTodo.id === id ){
    //       myTodo.completed = !myTodo.completed;
    //     }
    //     return myTodo
    //   })
    // })


    
  };

  deleteTodo =(id) =>{
    // this.setState({
    //   todos: [...this.state.todos.filter((myTodo) =>{
    //     return myTodo.id !==id
    //   })]
    // })

    firebase.database().ref('todos').child(id).remove()
  }
  
  addtodo = (title) => {
    // const newTodo = {

    //   id : Math.random(),
    //   title,
    //   completed: false
    // }

    // this.setState({
    //   todos: [...this.state.todos, newTodo]
    // })
    firebase.database().ref('todos').push().set({title:title})
  }

  render(){
  return (
    <div className="App">
      <Header/>
      <div className="todo-box">
        <RetrivedTodos todos={this.state.todos}
        markComplete={this.markComplete}
        deleteTodo={this.deleteTodo}
        />
      </div>
      <TodoForms addTodo={this.addtodo}/>
    </div>
  );
  }
}

export default App;
